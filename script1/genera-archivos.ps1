# Cantidad de archivos a generar
$numFiles = 10

# Función para generar contenido aleatorio
function RandomContent ($size) {
    return -join (1..$size | ForEach-Object { Get-Random -Minimum 65 -Maximum 91 | ForEach-Object { [char]$_ } })
}

for ($i=1; $i -le $numFiles; $i++) {
    $fileName = "archivo$i.txt"
    
    # Decide aleatoriamente el tamaño del contenido
    $size = Get-Random -Minimum 512 -Maximum 2049

    $content = RandomContent -size $size

    # Guarda el contenido en el archivo
    Set-Content -Path $fileName -Value $content
}

Write-Output "Se han creado $numFiles archivos .txt con contenido aleatorio."
