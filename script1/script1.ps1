# Obtener los archivos del directorio actual
$files = Get-ChildItem -File

# Filtrar los archivos que tengan un tamaño superior a 1024 bytes
Write-Output "Archivos mayores a 1024 bytes:"
$files | Where-Object { $_.Length -gt 1024 } | ForEach-Object {
    Write-Output "$($_.Name) - $($_.Length) bytes"
}
