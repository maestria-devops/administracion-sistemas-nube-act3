# administracion-sistemas-nube-act3

Programa un script que muestre un menú con las siguientes opciones, de manera que se ejecute la opción asociada al número que introduzca el usuario:

1. Listar los servicios arrancados.
2. Mostrar la fecha del sistema.
3. Ejecutar el Bloc de notas.
4. Ejecutar la Calculadora.
5. Salir.
