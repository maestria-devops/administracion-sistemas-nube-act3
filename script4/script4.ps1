function ShowMenu {
    Clear-Host
    Write-Output "Selecciona:"
    Write-Output "1. Listar los servicios arrancados."
    Write-Output "2. Mostrar la fecha del sistema."
    Write-Output "3. Ejecutar el Bloc de notas."
    Write-Output "4. Ejecutar la Calculadora."
    Write-Output "5. Salir."
}

$enterForContinueMsg = "Presiona enter para continuar..."
# Muestra el menu hasta que el usuario elija salir
do {
    ShowMenu

    $choice = Read-Host "Selecciona  (1-5)"

    switch ($choice) {
        "1" {
            $runningServices = Get-Service | Where-Object { $_.Status -eq "Running" }
            Write-Output $runningServices
            Read-Host $enterForContinueMsg
        }

        "2" {
            $currentDate = Get-Date
            Write-Output "Fecha Actual del Sistema: $currentDate"
            Read-Host $enterForContinueMsg
        }

        "3" {
            Start-Process "notepad.exe"
        }

        "4" {
            Start-Process "calc.exe"
        }

        "5" {
            Write-Output "Exiting..."
            exit
        }

        default {
            Write-Output "Selección invalida. El valor debe estar entre 1 y 5."
            Read-Host $enterForContinueMsg
        }
    }
} while ($choice -ne "5")
