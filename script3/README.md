# administracion-sistemas-nube-act3

Programa un script en PowerShell que muestre los discos duros con un porcentaje de espacio libre inferior a un parámetro dado. El script debe imprimir la letra de la unidad y los valores en GB de espacio libre y tamaño sin decimales. La expresión Get-WmiObject Win32_LogicalDisk recupera la información de los discos del sistema.