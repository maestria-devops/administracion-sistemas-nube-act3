param (
    [Parameter(Mandatory=$true)]
    [int]$MaxFreeSpace
)

# Obtener información sobre todos los discos duros del sistema
$drives = Get-WmiObject Win32_LogicalDisk -Filter "DriveType=3"

Write-Output "Discos duros con un porcentaje de espacio libre inferior a $MaxFreeSpace%:"
Write-Output ""

foreach ($drive in $drives) {
    # Calcular el porcentaje de espacio libre
    $freeSpace = ($drive.FreeSpace / $drive.Size) * 100

    # Comparar con el porcentaje maximo especificado
    if ($freeSpace -lt $MaxFreeSpace) { # Discos inferiores
        # Convertir valores a GB y redondear
        $freeSpaceGB = [Math]::Round($drive.FreeSpace / 1GB)
        $sizeGB = [Math]::Round($drive.Size / 1GB)

        # Imprimir resultados
        Write-Output "Unidad: $($drive.DeviceID) Total: $sizeGB GB, Libre: $freeSpaceGB GB $($freeSpace.ToString("F2"))%"
    }
}

Write-Output "Análisis completado."
