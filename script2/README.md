# administracion-sistemas-nube-act3

Escribe un script que renombre todos los ficheros con extensión JPG del directorio actual, añadiendo un prefijo con la fecha en formato año, mes, día.Por ejemplo, un fichero con nombre imagen1.jpg pasaría a llamarse 20200413-image1.jpg, si el script se ejecuta el 13 de abril de 2020.

Ejecutar primero el script "genera-archivos.ps1" para generar los archivos de prueba con extensión JPG