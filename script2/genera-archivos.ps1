# Definir el número de archivos a crear
$numFiles = 5

# Crear los archivos en un bucle
for ($i=1; $i -le $numFiles; $i++) {
    $fileName = "imagen$i.jpg"
    # Utiliza New-Item para crear un archivo vacío
    New-Item -Path ./$fileName -ItemType File
}

Write-Output "Se han creado $numFiles archivos JPG."
