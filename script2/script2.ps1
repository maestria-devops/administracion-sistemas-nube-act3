# Obtener la fecha actual en el formato deseado (año, mes, día)
$fecha = Get-Date -Format "yyyyMMdd"

# Obtener todos los archivos JPG del directorio actual
$archivosJPG = Get-ChildItem -Filter "*.jpg"

# Renombrar cada archivo añadiendo el prefijo de la fecha
foreach ($archivo in $archivosJPG) {
    $nuevoNombre = "${fecha}-$($archivo.Name)"
    Rename-Item -Path $archivo.FullName -NewName $nuevoNombre
}

Write-Output "Todos los archivos JPG han sido renombrados."
